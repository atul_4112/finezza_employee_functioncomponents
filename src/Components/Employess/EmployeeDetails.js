import React, { useState, useEffect } from "react";
import { getAllEmployee } from "../../APIList";
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Typography,
  Button,
  Alert,
  Box,
} from "@mui/material";
import "./employee.css";
import { NavLink } from "react-router-dom";
import Loading from "../Loader/Loading";
import { styled } from "@mui/system";
import { AddBox } from "@mui/icons-material";

const StyledButton = styled(Button)({
  marginLeft: "10px",
});

const EmployeeDetails = () => {
  const [empData, setEmpData] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const [error, setError] = useState("");


  const getAllEmployeeData = async () => {
    setIsLoading(true);
    try {
      const response = await getAllEmployee();
      setEmpData(response);
      setIsLoading(false);
    } catch (error) {
      setError(`Failed while fetching data..! ${error.message}`);
      setIsLoading(false);
    }
  };

  useEffect(() => {
    getAllEmployeeData();
  }, []);

  const renderAction = (data) => {
    return (
      <NavLink
        to={data ? `/emp/${data.id}` : "#"}
        // style={{ textDecoration: "none", color: "black" }}
      >
        <h3>
          {data.emp_title}
          {data.emp_name}
        </h3>
      </NavLink>
    );
  };
  const columns = [
    {
      id: "id",
      label: "ID",
      minWidth: 50,
      align: "left",
    },
    {
      id: "name",
      label: "Name",
      minWidth: 100,
      align: "left",
    },
    {
      id: "emp_email",
      label: "Email",
      minWidth: 150,
      align: "left",
    },
    {
      id: "emp_profileUrl",
      label: "Profile",
      minWidth: 100,
      format: (value) => (
        <img
          style={{ width: "80px", height: "80px", objectFit: "cover" }}
          src={value}
          alt="employee profile"
        />
      ),
    },
  ];

  return (
    <div className="employeetable">
      <Box className="addbtn">
        <Typography variant="h5" gutterBottom>
          Employee details
        </Typography>{" "}
        <NavLink
          to="/emp/add"
          style={{ textDecoration: "none", color: "white" }}
        >
          <StyledButton
            variant="contained"
            color="primary"
            startIcon={<AddBox />}
          >
            Add Employee
          </StyledButton>
        </NavLink>
      </Box>

      {isLoading && <Loading />}
      {error && <Alert severity="error">{error}</Alert>}
      {!isLoading && !error && (
        <TableContainer component={Paper}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead>
              <TableRow>
                {columns.map((column) => (
                  <TableCell
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {empData.map((row) => (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.id}>
                  {columns.map((column) => {
                    const value = row[column.id];
                    return (
                      <TableCell key={column.id} align={column.align}>
                        {column.format ? column.format(value) : value}
                        {column.id === "name" && renderAction(row)}
                      </TableCell>
                    );
                  })}
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      )}
    </div>
  );
};

export default EmployeeDetails;
