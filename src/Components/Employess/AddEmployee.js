import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

import {
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Button,
  Card,
  InputLabel,
  MenuItem,
  Select,
  Alert,
  FormGroup,
  Checkbox,
} from "@mui/material";
import { addEmployee } from "../../APIList";
import "./employee.css";
import ReplyOutlinedIcon from "@mui/icons-material/ReplyOutlined";
// import CheckboxGroup from "./Checkbox";

const AddEmployee = () => {
  const [error, setError] = useState("");
  const [title, setTitle] = useState("");
  const navigate = useNavigate();
  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };
  const [selectedItems, setSelectedItems] = useState([]);

  const handleChange = (event) => {
    const { value } = event.target;
    setSelectedItems(value);
  };

  const [checked, setChecked] = useState([]);
  const options = [
    { value: "frontend", label: "Frontend" },
    { value: "backend", label: "Backend" },
    { value: "fullstack", label: "Fullstack" },
    { value: "HR", label: "HR" },
  ];
  const handleChangeCheck = (event) => {
    const { value } = event.target;
    const index = checked.indexOf(value);
    const newChecked = [...checked];

    if (index === -1) {
      newChecked.push(value);
    } else {
      newChecked.splice(index, 1);
    }

    setChecked(newChecked);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const values = Object.fromEntries(formData.entries());
    console.log(values);
    try {
      await addEmployee(values);
      setError("");
      alert("Employee created successfully.");
      setTimeout(() => {
        window.location.href = "/";
      }, "1000");
    } catch (error) {
      setError(
        `${error.response.data.message} Please checkout this ${error.response.data.errors}`
      );
    }
  };

  return (
    <div className="employeetable">
      <Card>
        {error && <Alert severity="error">{error}</Alert>}
        <form onSubmit={handleSubmit} className="addemployeeform">
          <h2>Add new employee</h2>
          <div className="formtitle">
            <FormControl
              margin="normal"
              style={{ width: "10%", marginRight: "2%" }}
            >
              <InputLabel id="title-label">Title</InputLabel>
              <Select
                labelId="title-label"
                id="title"
                name="emp_title"
                label="Title"
                value={title}
                onChange={handleTitleChange}
              >
                <MenuItem value={"Mr."}>Mr.</MenuItem>
                <MenuItem value={"Ms."}>Ms.</MenuItem>
                <MenuItem value={"Mrs."}>Mrs.</MenuItem>
                <MenuItem value={"Msrs."}>Msrs.</MenuItem>
              </Select>
            </FormControl>
            <FormControl fullWidth margin="normal">
              <TextField
                label="Name"
                name="emp_name"
                required
                variant="outlined"
              />
            </FormControl>
          </div>
          <FormControl fullWidth margin="normal">
            <TextField
              label="Profile Picture"
              name="emp_profileUrl"
              required
              variant="outlined"
            />
          </FormControl>
          <FormControl fullWidth margin="normal">
            <TextField
              label="Email"
              name="emp_email"
              type="email"
              required
              variant="outlined"
            />
          </FormControl>
          <FormControl fullWidth margin="normal">
            <TextField
              label="Contact Number"
              name="emp_contact"
              type="number"
              required
              onInput={(e) => {
                e.target.value = Math.max(0, parseInt(e.target.value))
                  .toString()
                  .slice(0, 10);
              }}
              variant="outlined"
            />
          </FormControl>
          <FormControl fullWidth margin="normal">
            <TextField
              label="Address"
              name="emp_address"
              required
              variant="outlined"
              multiline
              rows={4}
            />
          </FormControl>
          <FormControl fullWidth margin="normal">
            <TextField
              label="Pincode"
              name="emp_pincode"
              type="number"
              required
              onInput={(e) => {
                e.target.value = Math.max(0, parseInt(e.target.value))
                  .toString()
                  .slice(0, 6);
              }}
              variant="outlined"
            />
          </FormControl>
          <FormControl fullWidth margin="normal">
            <TextField
              id="date"
              label="Birthday"
              type="date"
              name="emp_dob"
              sx={{ width: 220 }}
              InputLabelProps={{
                shrink: true,
              }}
              inputProps={{
                max: new Date().toISOString().split("T")[0],
              }}
            />
          </FormControl>
          <FormControl>
            <InputLabel id="education-label">Education</InputLabel>
            <Select
              multiple
              value={selectedItems}
              onChange={handleChange}
              name="emp_education"
              required
              label="Education"
            >
              <MenuItem value={"ssc"}>SSC</MenuItem>
              <MenuItem value={"hsc"}>HSC</MenuItem>
              <MenuItem value={"diploma"}>Diploma</MenuItem>
              <MenuItem value={"undergraduation"}>Under graduation</MenuItem>
              <MenuItem value={"postgraduation"}>Post graduation</MenuItem>
            </Select>
          </FormControl>

          <FormControl component="fieldset" margin="normal">
            <FormLabel component="legend">Gender</FormLabel>
            <RadioGroup defaultValue="male" name="emp_gender">
              <FormControlLabel value="male" control={<Radio />} label="Male" />
              <FormControlLabel
                value="female"
                control={<Radio />}
                label="Female"
              />
              <FormControlLabel
                value="other"
                control={<Radio />}
                label="Other"
              />
            </RadioGroup>
          </FormControl>
          <FormControl component="fieldset" margin="normal">
            <FormLabel component="legend">Marital Status</FormLabel>
            <RadioGroup
              defaultValue="single"
              name="emp_martialStatus"
              className="radiobtn"
            >
              <FormControlLabel
                value="single"
                control={<Radio />}
                label="Single"
              />
              <FormControlLabel
                value="married"
                control={<Radio />}
                label="Married"
              />
              <FormControlLabel
                value="divorced"
                control={<Radio />}
                label="Divorced"
              />
            </RadioGroup>
          </FormControl>
          <FormControl>
            <FormLabel component="legend">Tag</FormLabel>
            <FormGroup>
              {options.map((option) => (
                <FormControlLabel
                  key={option.value}
                  control={
                    <Checkbox
                      checked={checked.indexOf(option.value) !== -1}
                      onChange={handleChangeCheck}
                      name="emp_tag"
                      value={option.value}
                    />
                  }
                  label={option.label}
                />
              ))}
            </FormGroup>
          </FormControl>
          <FormControl>
            <div className="btnupdate">
              <Button
                type="submit"
                variant="contained"
                color="primary"
                size="large"
              >
                Submit
              </Button>
              <Button
                variant="contained"
                color="primary"
                size="large"
                startIcon={<ReplyOutlinedIcon />}
                onClick={() => navigate("/")}
              >
                Back
              </Button>
            </div>
          </FormControl>
        </form>
      </Card>
    </div>
  );
};
export default AddEmployee;
