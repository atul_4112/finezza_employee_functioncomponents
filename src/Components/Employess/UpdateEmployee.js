import React, { useState, useEffect } from "react";
import { useParams, useNavigate } from "react-router-dom";
import {
  TextField,
  FormControl,
  FormLabel,
  RadioGroup,
  FormControlLabel,
  Radio,
  Button,
  InputLabel,
  MenuItem,
  Select,
  Box,
  Alert,
} from "@mui/material";
import DeleteIcon from "@mui/icons-material/Delete";
import UpgradeOutlinedIcon from "@mui/icons-material/UpgradeOutlined";
import ReplyOutlinedIcon from "@mui/icons-material/ReplyOutlined";
import {
  getEmployeeById,
  updateEmployeeByID,
  deleteEmployeeById,
} from "../../APIList";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogContent from "@mui/material/DialogContent";
import DialogContentText from "@mui/material/DialogContentText";
import DialogTitle from "@mui/material/DialogTitle";
import Loading from "../Loader/Loading";

function UpdateEmployee() {
  const [empData, setEmpData] = useState([]);
  const { id } = useParams();
  const [error, setError] = useState("");
  const [title, setTitle] = useState("");
  const [isLoading, setIsLoading] = useState(false);
  const [open, setOpen] = React.useState(false);
  const navigate = useNavigate();
  const [selectedItems, setSelectedItems] = useState([]);

  const handleChange = (event) => {
    const { value } = event.target;
    setSelectedItems(value);
  };

  const renderSelected = () => {
    return selectedItems.join(" ");
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleTitleChange = (event) => {
    setTitle(event.target.value);
  };

  useEffect(() => {
    const fetchEmployee = async () => {
      setIsLoading(true);
      try {
        const data = await getEmployeeById(id);
        setEmpData(data);
        setSelectedItems([data[0].emp_education]);
        setIsLoading(false);
        //  console.log()
      } catch (error) {
        setError(error.response.data.message);
        setIsLoading(false);
      }
    };

    fetchEmployee();
  }, [id]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData(e.target);
    const values = Object.fromEntries(formData.entries());
    setIsLoading(true);
    try {
      await updateEmployeeByID(id, values);
      setError("");
      alert("Employee upated successfully.");
      setTimeout(() => {
        window.location.href = "/";
      }, "600");
      this.formRef.current.resetFields();
      setIsLoading(false);
    } catch (error) {
      setError(
        `${error.response.data.message} Please checkout this ${error.response.data.errors}`
      );
      setIsLoading(false);
    }
  };

  const handleDelete = async () => {
    setIsLoading(true);
    try {
      const data = await deleteEmployeeById(id);
      setEmpData(data);
      handleClose();
      alert("Employee Deleted successfully.");
      setTimeout(() => {
        window.location.href = "/";
      }, "600");
      setIsLoading(false);
    } catch (error) {
      setError(
        `${error.response.data.message} Please checkout this ${error.response.data.errors}`
      );
      setIsLoading(false);
    }
  };

  return (
    <div className="employeetable">
      {error && <Alert severity="error">{error}</Alert>}
      {isLoading && <Loading />}
      {empData &&
        empData.map((data) => (
          <form
            key={data.id}
            onSubmit={handleSubmit}
            className="addemployeeform"
          >
            <h2>Update employee</h2>
            <Box className="formtitle">
              <Box>
                <FormControl margin="normal" style={{ width: "15%" }}>
                  <InputLabel>Title</InputLabel>
                  <Select
                    labelId="title-label"
                    id="title"
                    name="emp_title"
                    label="Title"
                    value={title || data.emp_title}
                    onChange={handleTitleChange}
                  >
                    <MenuItem value={"Mr."}>Mr.</MenuItem>
                    <MenuItem value={"Ms."}>Ms.</MenuItem>
                    <MenuItem value={"Mrs."}>Mrs.</MenuItem>
                    <MenuItem value={"Msrs."}>Msrs.</MenuItem>
                  </Select>
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <TextField
                    label="Name"
                    name="emp_name"
                    required
                    variant="outlined"
                    defaultValue={data.emp_name}
                  />
                </FormControl>
                <FormControl fullWidth margin="normal">
                  <TextField
                    label="Profile Picture"
                    name="emp_profileUrl"
                    required
                    variant="outlined"
                    multiline
                    rows={4}
                    defaultValue={data.emp_profileUrl}
                  />
                </FormControl>
              </Box>
              <Box
                component="img"
                sx={{
                  height: 300,
                  width: 280,
                }}
                alt={data.emp_profileUrl}
                src={data.emp_profileUrl}
              />
            </Box>
            <FormControl fullWidth margin="normal">
              <TextField
                label="Email"
                name="emp_email"
                type="email"
                required
                variant="outlined"
                defaultValue={data.emp_email}
              />
            </FormControl>
            <FormControl fullWidth margin="normal">
              <TextField
                label="Contact Number"
                name="emp_contact"
                type="number"
                required
                onInput={(e) => {
                  e.target.value = Math.max(0, parseInt(e.target.value))
                    .toString()
                    .slice(0, 10);
                }}
                variant="outlined"
                defaultValue={data.emp_contact}
              />
            </FormControl>
            <FormControl fullWidth margin="normal">
              <TextField
                label="Address"
                name="emp_address"
                required
                variant="outlined"
                multiline
                rows={4}
                defaultValue={data.emp_address}
              />
            </FormControl>
            <FormControl fullWidth margin="normal">
              <TextField
                label="Pincode"
                name="emp_pincode"
                type="number"
                required
                onInput={(e) => {
                  e.target.value = Math.max(0, parseInt(e.target.value))
                    .toString()
                    .slice(0, 6);
                }}
                variant="outlined"
                defaultValue={data.emp_pincode}
              />
            </FormControl>
            <FormControl fullWidth margin="normal">
              <TextField
                id="date"
                label="Birthday"
                type="date"
                name="emp_dob"
                sx={{ width: 220 }}
                InputLabelProps={{
                  shrink: true,
                }}
                defaultValue={data.emp_dob}
              />
            </FormControl>
            <FormControl>
              <InputLabel id="education-label">Education</InputLabel>
              <Select
                multiple
                value={selectedItems}
                onChange={handleChange}
                name="emp_education"
                required
                label="Education"
                renderValue={renderSelected}
              >
                <MenuItem value={"ssc"}>SSC</MenuItem>
                <MenuItem value={"hsc"}>HSC</MenuItem>
                <MenuItem value={"diploma"}>Diploma</MenuItem>
                <MenuItem value={"undergraduation"}>Under graduation</MenuItem>
                <MenuItem value={"postgraduation"}>Post graduation</MenuItem>
              </Select>
            </FormControl>

            <FormControl component="fieldset" margin="normal">
              <FormLabel component="legend">Gender</FormLabel>
              <RadioGroup defaultValue={data.emp_gender} name="emp_gender">
                <FormControlLabel
                  value="male"
                  control={<Radio />}
                  label="Male"
                />
                <FormControlLabel
                  value="female"
                  control={<Radio />}
                  label="Female"
                />
                <FormControlLabel
                  value="other"
                  control={<Radio />}
                  label="Other"
                />
              </RadioGroup>
            </FormControl>
            <FormControl component="fieldset" margin="normal">
              <FormLabel component="legend">Marital Status</FormLabel>
              <RadioGroup
                defaultValue={data.emp_martialStatus}
                name="emp_martialStatus"
                className="radiobtn"
              >
                <FormControlLabel
                  value="single"
                  control={<Radio />}
                  label="Single"
                />
                <FormControlLabel
                  value="married"
                  control={<Radio />}
                  label="Married"
                />
                <FormControlLabel
                  value="divorced"
                  control={<Radio />}
                  label="Divorced"
                />
              </RadioGroup>
            </FormControl>
            <FormControl>
              <Box className="btnupdate">
                <Button
                  type="submit"
                  variant="contained"
                  color="primary"
                  size="large"
                  startIcon={<UpgradeOutlinedIcon />}
                >
                  Update
                </Button>
                <Button
                  variant="contained"
                  color="primary"
                  size="large"
                  startIcon={<ReplyOutlinedIcon />}
                  onClick={() => navigate("/")}
                >
                  Back
                </Button>
                <Button
                  size="large"
                  variant="outlined"
                  startIcon={<DeleteIcon />}
                  onClick={handleClickOpen}
                  style={{ background: "red", border: "none", color: "white" }}
                >
                  Delete
                </Button>

                <div>
                  <Dialog
                    open={open}
                    onClose={handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                  >
                    <DialogTitle id="alert-dialog-title">Delete</DialogTitle>
                    <DialogContent>
                      <DialogContentText id="alert-dialog-description">
                        You sure..! Want to delete?
                      </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                      <Button onClick={handleClose}>No</Button>
                      <Button onClick={handleDelete} autoFocus>
                        Yes
                      </Button>
                    </DialogActions>
                  </Dialog>
                </div>
              </Box>
            </FormControl>
          </form>
        ))}
    </div>
  );
}

export default UpdateEmployee;
