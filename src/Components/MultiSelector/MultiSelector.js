import * as React from "react";
import { useState } from "react";
import { makeStyles } from "@mui/styles";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  },
  chips: {
    display: "flex",
    flexWrap: "wrap",
  },
  chip: {
    margin: 2,
  },
  noLabel: {
    marginTop: theme.spacing(3),
  },
}));

export default function MultiSelectDropdown() {
  const classes = useStyles();
  const [selectedItems, setSelectedItems] = useState([]);

  const handleChange = (event) => {
    const { value } = event.target;
    setSelectedItems(value);
  };

  return (
    <div>
      <Select
        multiple
        value={selectedItems}
        onChange={handleChange}
        className={classes.formControl}
      >
        <MenuItem value="item1">Item 1</MenuItem>
        <MenuItem value="item2">Item 2</MenuItem>
        <MenuItem value="item3">Item 3</MenuItem>
        <MenuItem value="item4">Item 4</MenuItem>
      </Select>
    </div>
  );
}
