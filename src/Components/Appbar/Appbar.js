import React from "react";
import { AppBar, Toolbar, Typography } from "@mui/material";
import { makeStyles } from "@mui/styles";
import { NavLink } from "react-router-dom";

const useStyles = makeStyles((theme) => ({
  title: {
    flexGrow: 1,
  },
}));

function Appbar() {
  const classes = useStyles();

  return (
    <AppBar position="sticky">
      <Toolbar>
        <Typography variant="h6" className={classes.title}>
          <NavLink to="/" style={{ textDecoration: "none", color: "white" }}>
            Finezza
          </NavLink>
        </Typography>
      </Toolbar>
    </AppBar>
  );
}

export default Appbar;
