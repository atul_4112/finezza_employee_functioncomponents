import axios from "axios";
const apibase = process.env.REACT_APP_BASE_URL;

export const getAllEmployee = async () => {
  try {
    const response = await axios.get(`${apibase}/emp/getAllEmployee`);
    return response.data;
  } catch (error) {
    throw error;
  }
};

export const addEmployee = async (values) => {
  try {
    const response = await axios.post(`${apibase}/emp/addemployee`, values);
    const data = response.data;
    return data;
  } catch (error) {
    throw error;
  }
};

export const getEmployeeById = async (id) => {
  try {
    const response = await axios.get(`${apibase}/emp/${id}`);
    const data = response.data;
    return [data];
  } catch (error) {
    throw error;
  }
};

export const updateEmployeeByID = async (id, values) => {
  try {
    const response = await axios.put(`${apibase}/emp/${id}`, values);
    const data = response.data;
    return [data];
  } catch (error) {
    throw error;
  }
};

export const deleteEmployeeById = async (id) => {
  try {
    const response = await axios.delete(`${apibase}/emp/${id}`);
    const data = response.data;
    return [data];
  } catch (error) {
    throw error;
  }
};
