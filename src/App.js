// import { Route } from "react-router-dom";
import Appbar from "./Components/Appbar/Appbar";
// import EmployeeDetails from "./Components/Employess/EmployeeDetails";

// function App() {
//   return (
//     <div className="App">
//
//       <EmployeeDetails />
//       <Route>
//         <Routes></Routes>
//       </Route>
//     </div>
//   );
// }

// export default App;

import { useRoutes } from "react-router-dom";
import EmployeeDetails from "./Components/Employess/EmployeeDetails";
import NotFoundPage from "./Components/Employess/PagenotFound";
import UpdateEmployee from "./Components/Employess/UpdateEmployee";
import AddEmployee from "./Components/Employess/AddEmployee";

const App = () => {
  const routes = useRoutes([
    { path: "/", element: <EmployeeDetails /> },
    { path:"/emp/add", element:<AddEmployee />},
    { path: "/emp/:id", element: <UpdateEmployee /> },
    { path: "*", element: <NotFoundPage /> },
  ]);

  return (
    <div>
      <Appbar />
      {routes}
    </div>
  );
};

export default App;
